package demo.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="show_actor_t")
public class ShowActor implements Serializable{

	private static final long serialVersionUID = -5063362120291967608L;
	
	@Id
	@GeneratedValue
	@Column(name="show_actor_id")
	private Integer id;
	
	@ManyToOne
	@JoinColumn(name="actor_id")
	private Actor actor;
	
	@ManyToOne
	@JoinColumn(name="show_id")
	private Show show;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Actor getActor() {
		return actor;
	}

	public void setActor(Actor actor) {
		this.actor = actor;
	}

	public Show getShow() {
		return show;
	}

	public void setShow(Show show) {
		this.show = show;
	}
	
}
