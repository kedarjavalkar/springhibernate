package demo.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="category_t")
public class Category implements Serializable{

	private static final long serialVersionUID = 255683858490299493L;

	@Id
	@GeneratedValue
	@Column(name="category_id")
	private Integer id;
	
	@Column(name="name", length=50, nullable = false)
	private String name;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	
}
