package demo.bi.show;

import java.util.List;

import demo.entity.Actor;
import demo.entity.Show;

public interface ShowManager {

	public List<Show> getShowList();
	public Show getShowInfoById(Integer showId);
	public List<Actor> getActorsById(Integer showId);
	public void saveShow(Show showObj);
	
}
