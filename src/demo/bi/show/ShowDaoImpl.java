package demo.bi.show;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import demo.entity.Actor;
import demo.entity.Show;

@Repository("ShowDao")
public class ShowDaoImpl implements ShowDao{

	@Autowired
	SessionFactory sessionFactory;
	
	@Override
	@SuppressWarnings("unchecked")
	public List<Show> getShowList(){

		List<Show> showList = null;
		Session session = sessionFactory.openSession();
		showList = session.createCriteria(Show.class)
				.addOrder(Order.desc("lastUpdated")).list();
		session.close();

		return showList;
	}
	
	@Override
	public Show getShowInfoById(Integer showId){
		
		Show showObj = new Show();
		Session session = sessionFactory.openSession();
		showObj = (Show) session.createCriteria(Show.class)
							.add(Restrictions.eq("id", showId)).list().get(0);
		session.close();
		
		return showObj;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<Actor> getActorsById(Integer showId){
		
		List<Actor> actorList = null; 
		Session session = sessionFactory.openSession();
		Query query = session.createSQLQuery("select a.* from actor_t as a inner join show_actor_t as sa "+ 
												"on a.actor_id=sa.actor_id where sa.show_id = :showId")
						.addEntity(Actor.class)
						.setParameter("showId", showId);
		actorList = query.list();
		
		return actorList;
	}
	@Override
	public void saveShow(Show showObj){
		
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		session.saveOrUpdate(showObj);
		tx.commit();
		session.close();
		
	}
}
