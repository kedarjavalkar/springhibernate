package demo.bi.show;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import demo.entity.Actor;
import demo.entity.Show;

@Service("ShowManager")
public class ShowManagerImpl implements ShowManager {

	@Autowired
	ShowDao showDao;

	@Override
	public List<Show> getShowList(){
		return showDao.getShowList();	
	}
	@Override
	public Show getShowInfoById(Integer showId){
		return showDao.getShowInfoById(showId);
	}
	@Override
	public List<Actor> getActorsById(Integer showId){
		return showDao.getActorsById(showId);
	}
	@Override
	public void saveShow(Show showObj){
		showDao.saveShow(showObj);
	}
	
}
