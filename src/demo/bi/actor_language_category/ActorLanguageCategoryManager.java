package demo.bi.actor_language_category;

import java.util.List;

import demo.entity.Actor;
import demo.entity.Category;
import demo.entity.Language;
import demo.entity.ShowActor;

public interface ActorLanguageCategoryManager {

	public List<Actor> getActorsList(String queryParam);
	public List<Language> getLanguageList();
	public List<Category> getCategoryList();
	public void saveToList(ShowActor showActor);
}
