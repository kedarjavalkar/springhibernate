package demo.bi.actor_language_category;

import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import demo.entity.Actor;
import demo.entity.Category;
import demo.entity.Language;
import demo.entity.ShowActor;


@Transactional
@Repository("ActorDao")
public class ActorLanguageCategoryDaoImpl implements ActorLanguageCategoryDao {

	@Autowired
	SessionFactory sessionFactory;
	
	
	@Override
	@SuppressWarnings("unchecked")
	public List<Actor> getActorsList(String queryParam){
		
		List<Actor> actorList = null;
		Session session = sessionFactory.openSession();
		actorList = session.createCriteria(Actor.class)
					.add(Restrictions.disjunction()
						.add(Restrictions.like("firstName", "%"+queryParam+"%"))
						.add(Restrictions.like("lastName", "%"+queryParam+"%"))
					).list();
		
		return actorList;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<Language> getLanguageList(){
		
		List<Language> languageList = null;
		Session session = sessionFactory.openSession();
		languageList = (List<Language>) session.createCriteria(Language.class).list();
		
		return languageList;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<Category> getCategoryList(){
		List<Category> categorylList = null;
		Session session = sessionFactory.openSession();
		categorylList = (List<Category>) session.createCriteria(Category.class).list();
		
		return categorylList;
		
	}
	
	@Override
	public void saveToList(ShowActor showActor){
		
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		session.saveOrUpdate(showActor);
		tx.commit();
		session.close();
	}
	
}
