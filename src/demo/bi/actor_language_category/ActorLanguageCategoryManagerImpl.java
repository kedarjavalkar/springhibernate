package demo.bi.actor_language_category;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import demo.entity.Actor;
import demo.entity.Category;
import demo.entity.Language;
import demo.entity.ShowActor;

@Service("ActorManager")
public class ActorLanguageCategoryManagerImpl implements ActorLanguageCategoryManager {

	@Autowired
	ActorLanguageCategoryDao actorLanguageCategoryDao;
	
	@Override
	public List<Actor> getActorsList(String queryParam){
		return actorLanguageCategoryDao.getActorsList(queryParam);
	}
	@Override
	public List<Language> getLanguageList(){
		return actorLanguageCategoryDao.getLanguageList();
	}
	@Override
	public List<Category> getCategoryList(){
		return actorLanguageCategoryDao.getCategoryList();
	}
	@Override
	public void saveToList(ShowActor showActor){
		actorLanguageCategoryDao.saveToList(showActor);
	}
	
}
