package demo.controller;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import demo.bi.actor_language_category.ActorLanguageCategoryManager;
import demo.bi.show.ShowManager;
import demo.entity.Actor;
import demo.entity.Category;
import demo.entity.Language;
import demo.entity.Show;
import demo.entity.ShowActor;


@Controller
public class SpringDemoController {

	@Autowired
	ShowManager showManager;
	@Autowired
	ActorLanguageCategoryManager actorLanguageCategoryManager;
	
	@RequestMapping(value="getshowList.io",method=RequestMethod.GET)
	@ResponseBody String getshowList(HttpServletRequest request) throws ParseException{

		/*SimpleDateFormat dateFormatGmt = new SimpleDateFormat("dd-MMMM-yyyy HH:mm");
		dateFormatGmt.setTimeZone(TimeZone.getTimeZone("GMT"));
		SimpleDateFormat dateFormatLocal = new SimpleDateFormat("dd-MMMM-yyyy HH:mm");
		Date saveDate = new Date();
		saveDate = dateFormatLocal.parse(dateFormatGmt.format(new Date()));
		System.out.println(saveDate);
		System.out.println("-----------------------------------------------------------------------");

	    SimpleDateFormat f = new SimpleDateFormat("dd-MMMM-yyyy HH:mm");
	    f.setTimeZone(TimeZone.getTimeZone("GMT"));
	    System.out.println(f.format(new Date()));
	    f.setTimeZone(TimeZone.getTimeZone("GMT+5:30"));

	    System.out.println(f.format(new Date()));
	    System.out.println("-----------------------------------------------------------------------");
		return "";*/
		//System.out.println(new Date());
		
		JsonArray jsonArray = new JsonArray();
		List<Show> showList = showManager.getShowList();
		for (Show show : showList) {
			JsonObject jsonObj = new JsonObject();
			jsonObj.addProperty("showId", show.getId());
			jsonObj.addProperty("title", show.getTitle());
			//jsonObj.addProperty("description", show.getDescription());
			jsonObj.addProperty("category",show.getCategory().getName());
			jsonObj.addProperty("language",show.getLanguage().getName());
			jsonObj.addProperty("length", show.getLength());
			jsonObj.addProperty("releaseYear", show.getReleaseYear());
			//jsonObj.addProperty("lastUpdated", (show.getLastUpdated()==null)?null:show.getLastUpdated().toString());
			jsonArray.add(jsonObj);
		}

		return new Gson().toJson(jsonArray);
	}
	
	@RequestMapping(value="getShowInfo.io",method=RequestMethod.GET)
	@ResponseBody String getShowInfo(HttpServletRequest request){
		
		Integer showId = Integer.parseInt(request.getParameter("showId"));
		Show showObj = showManager.getShowInfoById(showId);
		List<Actor> actorList = showManager.getActorsById(showId);
		String actorNames="";
		for (int i=0; i<actorList.size(); i++){
			if(i==actorList.size()-1)
				actorNames+=actorList.get(i).getFirstName()+" "+actorList.get(i).getLastName();
			else if(i==(actorList.size()-2))
				actorNames+=actorList.get(i).getFirstName()+" "+actorList.get(i).getLastName()+" & ";
			else
				actorNames+=actorList.get(i).getFirstName()+" "+actorList.get(i).getLastName()+", ";
		}

		JsonObject jsonObj = new JsonObject();
		jsonObj.addProperty("title", showObj.getTitle());
		jsonObj.addProperty("description", showObj.getDescription());
		jsonObj.addProperty("actors", actorNames);
		
		return new Gson().toJson(jsonObj);
	}
	
	@RequestMapping(value="getActorsList.io",method=RequestMethod.GET)
	@ResponseBody String getActorsList(HttpServletRequest request){
		
		JsonArray jsonArray = new JsonArray();
		
		String queryParam = request.getParameter("k");
		if(queryParam.equals(""))
			return new Gson().toJson(jsonArray);
		
		List<Actor> actorList = actorLanguageCategoryManager.getActorsList(queryParam.trim());
		for (Actor actor : actorList) {
			JsonObject jsonObj = new JsonObject();
			jsonObj.addProperty("actorId", actor.getId());
			jsonObj.addProperty("actorName", actor.getFirstName()+" "+actor.getLastName());
			jsonArray.add(jsonObj);
		}
		
		return new Gson().toJson(jsonArray);
	}
	
	@RequestMapping(value="getLanguageList.io",method=RequestMethod.GET)
	@ResponseBody String getLanguageList(HttpServletRequest request){
		
		JsonArray jsonArray = new JsonArray();
		
		List<Language> languageList = actorLanguageCategoryManager.getLanguageList();
		for (Language language : languageList) {
			JsonObject jsonObj = new JsonObject();
			jsonObj.addProperty("languageId", language.getId());
			jsonObj.addProperty("languageName", language.getName());
			jsonArray.add(jsonObj);
		}
		
		return new Gson().toJson(jsonArray);
	}
	
	@RequestMapping(value="getCategoryList.io",method=RequestMethod.GET)
	@ResponseBody String getCategoryList(HttpServletRequest request){
		
		JsonArray jsonArray = new JsonArray();
		
		List<Category> categoryList= actorLanguageCategoryManager.getCategoryList();
		for (Category category : categoryList) {
			JsonObject jsonObj = new JsonObject();
			jsonObj.addProperty("categoryId", category.getId());
			jsonObj.addProperty("categoryName", category.getName());
			jsonArray.add(jsonObj);
		}
		
		return new Gson().toJson(jsonArray);
	}
	
	@RequestMapping(value="saveToList.io",method=RequestMethod.POST)
	@ResponseBody boolean addToList(HttpServletRequest request){
		
		String title = request.getParameter("title");
		String description = request.getParameter("description");
		String languageSuggest = request.getParameter("languageSuggest");
		String length = request.getParameter("length");
		String year = request.getParameter("year");
		String categorySuggest = request.getParameter("categorySuggest");
		String actorSuggest = request.getParameter("actorSuggest");
		
		Show showObj = new Show();
		showObj.setTitle(title);
		showObj.setDescription(description);
		showObj.setLength(Integer.parseInt(length));
		showObj.setReleaseYear(Integer.parseInt(year));
		Language languageObj = new Language();
		languageObj.setId(Integer.parseInt(languageSuggest));
		showObj.setLanguage(languageObj);
		Category categoryObj = new Category();
		categoryObj.setId(Integer.parseInt(categorySuggest));
		showObj.setCategory(categoryObj);
		showObj.setLastUpdated(new Date());
		
		showManager.saveShow(showObj);
		
		String actorIds[] = actorSuggest.split(",");
		for (String id : actorIds) {
			ShowActor showActorObj = new ShowActor();
			showActorObj.setShow(showObj);
			Actor actorObj = new Actor();
			actorObj.setId(Integer.parseInt(id));
			showActorObj.setActor(actorObj);
			actorLanguageCategoryManager.saveToList(showActorObj);
		}
		
		return true;

	}
	
	
}
