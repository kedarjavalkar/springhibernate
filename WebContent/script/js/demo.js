$('document').ready(function(){
		
	$('[data-toggle="tooltip"]').tooltip();
	getShowList();
	
	$('#modelAddEdit').on('shown.bs.modal',function(event){
		var button = $(event.relatedTarget)
		var recipient = button.data('recipient');
		if(recipient.split('-')[0]=="Add"){
			$(this).find('.modal-title').text('Add to List');
			$(this).find('#btnAddEditModel').text('Add');
			$(this).find('.modal-dialog').addClass('modal-lg');
			$(this).find('.modal-body').load('addToList.io');
		}else if(recipient.split('-')[0]=="More"){
			$(this).find('.modal-title').text('');
			$(this).find('#btnAddEditModel').addClass('hide');
			fetchMoreInfo(recipient.split('-')[1]);
		}
	});
});

function getShowList(){
	
	$.ajax({
		url:'getshowList.io',
		type:'GET',
		dataType:'json',
		async:false,
		beforeSend:function(){
			magicBlurStart();
		},
		success:function(jsonArray){
			var str='<table id="tblShowList" class="table table-condensed table-hover" style="text-align: center;">'+
			'<thead><tr><th>#</th><th>Title</th><th>Category</th><th>Langauge</th>'+
			'<th>Length(Min)</th><th>Release Year</th>'+'</tr></thead>';
			
			str+='<tbody>';
			var i=0;
			var showId;
			$.each(jsonArray, function() {
				str+='<tr>';
				$.each(this, function(key, value) {
					if(key=='showId'){
						str+='<td>'+parseInt(++i)+'</td>';
						showId=value;
					}else if(key=='title'){
						str+='<td><button class="btn btn-link" data-toggle="modal" data-target="#modelAddEdit" data-backdrop="static" '+
						'data-recipient="More-'+showId+'">'+value+'</button></td>';
					}
					else
						str+='<td>'+value+'</td>';
				});
				str+='</tr>';
			});
			str+='</tbody>';
			str+='</table>';
			
			$('#divShowList').html(str).parent().removeClass('hide');
			$('#tblShowList').DataTable();
			
		},
		error:function(){
			magicError("Calling 'getshowList.io' ");
		},
		complete:function(){
			magicBlurStop();
		},
	});
}

function fetchMoreInfo(showId){

	$.ajax({
		url:'getShowInfo.io',
		data:{showId:showId},
		type:'GET',
		dataType:'json',
		beforeSend:function(){
			magicBlurStart();
		},
		success:function(json){
			
			$('#modelAddEdit').find('.modal-title').text(json['title']).parent().removeClass('hide');
			
			var str=
			'<form> <table class="table table-condensed table-hover">'+
			'<tr> <td> <div class="form-group">'+
			'			<label for="textAreaActors" class="control-label">Description</label>'+
			'			<textarea rows="3" cols="6" class="form-control" id="textAreaDescription" readonly></textarea>'+
			'		</div> </td> </tr>'+
			'<tr> <td> <div class="form-group">'+
			'			<label for="textAreaActors" class="control-label">Actors</label>'+
			'			<textarea rows="3" cols="6" class="form-control" id="textAreaActors" readonly></textarea>'+
			'		</div> </td> </tr>'+
			'</table> </form>';
			$('#modelAddEdit').find('.modal-body').html(str);
			
			$('#textAreaDescription').text(json['description']);
			$('#textAreaActors').text(json['actors']);

		},
		error:function(){
			magicError("Calling 'getShowInfo.io' ");
		},
		complete:function(){
			magicBlurStop();
		},
	});
}

