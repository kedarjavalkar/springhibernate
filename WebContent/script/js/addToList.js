

$('document').ready(function() {

	$('#btnAddEditModel').on('click', function(e) {
		$('#btnCenterAddEdit').click();
	});

	var languageJson;
	$.ajax({
		url:'getLanguageList.io',
		type:'GET',
		dataType:'json',
		async:false,
		success:function(jsonArray){ languageJson = jsonArray; },
		error:function(){ magicError("Calling 'getLanguageList.io' "); },
	});
	var inputLanguageSuggest = $('#inputLanguageSuggest').magicSuggest({
		typeDelay: 0,
		//method:'GET',
		//queryParam: 'k',
		data:languageJson,
		valueField: 'languageId',
		displayField: 'languageName',
		allowFreeEntries:false,
		selectFirst:true,
		maxSelection:1,
		//maxSuggestions:8,
		placeholder:'',
		//minChars:2, minCharsRenderer:function(v){return'Be more precise Spiderman!!';},
		maxEntryLength: 5, maxEntryRenderer: function(v){return 'TOO LONG DUMMY!!';},
		noSuggestionText: 'No result matching the term <strong>{{query}}</strong>',
		maxDropHeight: 145,
		useZebraStyle: true,
		toggleOnClick: true,
	});
	$(inputLanguageSuggest).on('selectionchange', function(){
		//alert(JSON.stringify(this.getValue()));
		$('#inputLanguageSuggest').data("languageSuggest",this.getValue());
	});

	var categoryJson;
	$.ajax({
		url:'getCategoryList.io',
		type:'GET',
		dataType:'json',
		async:false,
		success:function(jsonArray){ categoryJson = jsonArray; },
		error:function(){ magicError("Calling 'getCategoryList.io' "); },
	});
	var inputCategorySuggest = $('#inputCategorySuggest').magicSuggest({
		typeDelay: 0,
		//method:'GET',
		//queryParam: 'k',
		data:categoryJson,
		valueField: 'categoryId',
		displayField: 'categoryName',
		allowFreeEntries:false,
		selectFirst:true,
		maxSelection:1,
		//maxSuggestions:8,
		placeholder:'',
		//minChars:2, minCharsRenderer:function(v){return'Be more precise Spiderman!!';},
		maxEntryLength: 5, maxEntryRenderer: function(v){return 'TOO LONG DUMMY!!';},
		noSuggestionText: 'No result matching the term <strong>{{query}}</strong>',
		maxDropHeight: 145,
		useZebraStyle: true,
		toggleOnClick: true,
	});
	$(inputCategorySuggest).on('selectionchange', function(){
		//alert(JSON.stringify(this.getValue()));
		$('#inputCategorySuggest').data("categorySuggest",this.getValue());
	});
	
	var inputActorSuggest = $('#inputActorSuggest').magicSuggest({
		//typeDelay: 800,
		method:'GET',
		queryParam: 'k',
		data:'getActorsList.io',
		valueField: 'actorId',
		displayField: 'actorName',
		allowFreeEntries:false,
		selectFirst:true,
		maxSelection:null,
		maxSuggestions:8,
		placeholder:'',
		minChars:2, minCharsRenderer:function(v){return'Be more precise Spiderman!!';},
		maxEntryLength: 8, maxEntryRenderer: function(v){return 'TOO LONG DUMMY!!';},
		noSuggestionText: 'No result matching the term <strong>{{query}}</strong>',
		maxDropHeight: 145,
		useZebraStyle: true,
	});
	$(inputActorSuggest).on('selectionchange', function(){
		//alert(JSON.stringify(this.getValue()));
		$('#inputActorSuggest').data("actorSuggest",this.getValue());
	});
	
	$('#inputTitle').focus();
});

$('#formAddToList').on('submit', function(e) { //.validator()
	if (e.isDefaultPrevented()) {
		// do something when error
	} else {
		e.preventDefault(); //preventing form submit
		var languageSuggest = $('#inputLanguageSuggest').data("languageSuggest");
		var categorySuggest = $('#inputCategorySuggest').data("categorySuggest");
		var actorSuggest = $('#inputActorSuggest').data("actorSuggest");

		if(languageSuggest==null || languageSuggest==undefined || languageSuggest==''){
			$('#inputLanguageSuggest').find('input').focus();
			return;
		}
		if(categorySuggest==null || categorySuggest==undefined || categorySuggest==''){
			$('#inputCategorySuggest').find('input').focus();
			return;
		}
		if(actorSuggest==null || actorSuggest==undefined || actorSuggest==''){
			$('#inputActorSuggest').find('input').focus();
			return;
		}

		var title = $('#inputTitle').val().trim();
		var description = $('#textAreaDescription').val().trim();
		//languageSuggest = languageSuggest.toString();
		var length = $('#inputLength').val().trim();
		var year = $('#inputYear').val().trim();
		//categorySuggest = categorySuggest.toString();
		//actorSuggest = actorSuggest.toString();

		$.ajax({
			url:'saveToList.io',
			data:{title:title,description:description,length:length,year:year,
				languageSuggest:languageSuggest.toString(),categorySuggest:categorySuggest.toString(),actorSuggest:actorSuggest.toString()},
				type:'POST',
				dataType:'text',
				async:false,
				beforeSend:function(){
					magicBlurStart();
				},
				success:function(msg){
					if(msg){
						$('#modelAddEdit').modal('hide');
						getShowList();
						setTimeout(function(){
							magicSuccess("Contribution Noted...");
						},500);
					}
				},
				error:function(){
					magicError("Calling 'addToList.io' ");
				},
				complete:function(){
					magicBlurStop();
				},
		});
	}
});





