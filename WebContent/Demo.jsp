<!DOCTYPE html >
<html lang="en">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<jsp:include page="/common/includes/header.jsp"></jsp:include>
<title>Spring Hibernate</title>
</head>
<body>
	<div class="container-fluid" style="margin-bottom: 50px;">
		
		<div class="jumbotron" style="padding: 10px 50px; margin-top: 10px;">
			<h1 style="margin: 0px 0px;">Hello, world!</h1>
			<p> This is minimal example of what Spring Framework can produce !<br>
				It enables you to build applications from <a data-toggle="tooltip" title="" href="javascript:void(0)"
					data-original-title="Plain Old Java Objects">POJOs</a><br> <br>
				<!-- Button trigger modal -->
				<button type="button" class="btn btn-success btn-lg" data-toggle="modal" data-target="#modelAddEdit" 
					data-recipient="Add" style="margin-top: -15px;" data-backdrop="static">Add to List</button>
			</p>
		</div>

		<!-- ------------------------------------------------------------------------------------------------ -->

		<div class="panel panel-info hide" style="width: 75%; margin: 0 auto;">
			<div class="panel-heading">
				<h3 class="panel-title"><strong>List</strong></h3>
			</div>
			<div class="panel-body">Click on <strong>Title</strong> for more detailed information...</div>
			<div id="divShowList" style="margin:5px 15px;"></div>
		</div>


	</div>
	<jsp:include page="/common/includes/footer.jsp"></jsp:include>
	<script type="text/javascript" src="script/js/demo.js"></script>
</body>
</html>