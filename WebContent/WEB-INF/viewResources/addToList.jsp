

<form role="form" data-toggle="validator" id="formAddToList">
	<table class="table table-condensed table-hover">
		<tr><td> <div class="form-group">
					<label for="inputTitle" class="control-label">Title *</label>
					<input type="text" class="form-control" id="inputTitle" required>
					<div class="help-block with-errors"></div>
				</div> </td>
			<td> <div class="form-group">
					<label for="textAreaDescription" class="control-label">Description *</label>
					<textarea rows="3" cols="6" class="form-control" id="textAreaDescription" required></textarea>
					<div class="help-block with-errors"></div>
				</div> </td>
			<td style="width: 33%;"> <div class="form-group">
					<label for="inputLanguageSuggest" class="control-label">Language *</label>
					<input type="text" class="form-control" id="inputLanguageSuggest" required>
					<div class="help-block with-errors"></div>
				</div> </td> </tr>
		<tr><td> <div class="form-group">
					<label for="inputLeghth" class="control-label">Length (Min) *</label>
					<input type="text" class="form-control" id="inputLength" pattern="^([0-9]{1,3})$" maxlength="3" required>
					<div class="help-block with-errors">Only Numeric Value</div>
				</div> </td>
			<td> <div class="form-group">
					<label for="inputYear" class="control-label">Release Year *</label>
					<input type="text" class="form-control" id="inputYear" pattern="^([0-9]{4})$" maxlength="4" required>
					<div class="help-block with-errors">Only 4 Digit Numeric Value</div>
				</div> </td>
			<td style="width: 33%;"> <div class="form-group" style="width: 100%; margin: 0 auto;">
					<label for="inputCategorySuggest" class="control-label">Category *</label>
					<input type="text" class="form-control" id="inputCategorySuggest" required>
					<div class="help-block with-errors"></div>
				</div> </td> </tr>
		<tr><td colspan="3"> <div class="form-group" style="width: 70%; margin: 0 auto;">
					<label for="inputActorSuggest" class="control-label">Actors *</label>
					<input type="text" class="form-control" id="inputActorSuggest" required>
					<div class="help-block with-errors"></div>
				</div> </td> </tr>		
		<tr style="display: none;"><td colspan="3"> <div class="form-group">
    				<button type="submit" id="btnCenterAddEdit">Submit</button>
  				</div> </td> </tr> 
  		<!-- <tr> <td colspan="2">* <span style="font-size: 10px;">Required field</span></td> </tr> -->
	</table>
	<p class="pull-right">* <span style="font-size: 10px;">Required field</span></p>
</form>

<script src="script/js/addToList.js"></script>

<style type="text/css">
.modal-dialog{
    overflow-y: initial !important
}
.modal-body{
	/* height: 80%; */
    overflow-y: auto;
}
</style>
